# Game 1 - Synopsis

## “math”

- What name describes the core idea of the game
  - Learning math in funny way 

- What game mechanic or mechanics needs to be emphasized?
  - Math engines

## Short description

- What is the game (with few words)?
  - Its about learning mathematics

- What kind of game is it?
  - puzzle game

- What does the player do?
  - doing math

- What is the core idea, slogan or basic philosophy of the game?
  - math is like cooking meth 

- What makes the game interesting?
  - Name at least four main things in the game
     - hard tasks
     - different formulas
     - Boss battles
     - Games difficulty
 
## Game development

- Why are you making this game?
  - I wanna spread the funny world of math

- What is the core motivator for the player in this game?
 - Learning math in funny way.

- What is the background of this game?
 - mathematics

- What are the most prioritized goals for the development?
 - Making people easy to understand math

- Other goals you want to set for your team?
 - learn about math

- What is the unique part in this game that other games don’t have?
 - mathematics.

## Target group

- What kind of gamers play this game?
 - People who sucks at math

- Why do they want this game?
 - They need to pass tests

- What kind of games do they like?
 - any kind of

- Is there any other reasons why the target group is important?
 - No.

## Methods

- How will you develop the game?
 - Using unity. 

- Core methods, tools and/or softwares
 - math engines

- What technology are you using? (Game engine, etc)
 - Unity 5. 

- Does the technology affect the development? (Why and how?)
 - Not really. 

- Who is developing the game and what parts of it?
 - Santtu Boman (Coding)


- How long does it take to make this game?
 - l4 years
