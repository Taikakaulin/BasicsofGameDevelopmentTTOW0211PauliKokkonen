# Game 1 - Synopsis

## “Kalevala”

- What name describes the core idea of the game
  - The Story of Kalevala 

- What game mechanic or mechanics needs to be emphasized?
  - Movement and fighting mechanics are very important aspect of this game.

## Short description

- What is the game (with few words)?
  - This game tells a story about Kalevala.

- What kind of game is it?
  - Roguelike platformer.

- What does the player do?
  - He follows the storyline and beat enemys along the way.

- What is the core idea, slogan or basic philosophy of the game?
  - To tell the story of Kalevala 

- What makes the game interesting?
  - Name at least four main things in the game
     - Smooth platforming
     - Epic story
     - Boss battles
     - Games difficulty
 
## Game development

- Why are you making this game?
  - Because we think that Kalevala's story would make a great game.

- What is the core motivator for the player in this game?
 - Overcome obstacles and continue story.

- What is the background of this game?
 - Kalevala

- What are the most prioritized goals for the development?
 - Make it very pleasant gaming exprience, meaning that the movement feels great and fighting also is smooth.

- Other goals you want to set for your team?
 - Learn lot about unity programming.

- What is the unique part in this game that other games don’t have?
 - Unique storytelling.

## Target group

- What kind of gamers play this game?
 - Gamers who enjoy solid single player games.

- Why do they want this game?
 - Story.

- What kind of games do they like?
 - Roguelike.

- Is there any other reasons why the target group is important?
 - No.

## Methods

- How will you develop the game?
 - Using unity. 

- Core methods, tools and/or softwares
 - Movement methods and camera follow of the character.

- What technology are you using? (Game engine, etc)
 - Unity 5. 

- Does the technology affect the development? (Why and how?)
 - Not really. 

- Who is developing the game and what parts of it?
 - Santtu Boman (Coding)
 - Pauli Kokkonen (Coding)
 - Antti Maaheimo (Art Works)

- How long does it take to make this game?
 - lets see..
