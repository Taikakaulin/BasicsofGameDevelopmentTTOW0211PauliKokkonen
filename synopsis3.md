# Game 3 - Synopsis

## “Fighter”

- What name describes the core idea of the game
  - Its idea is fighting

- What game mechanic or mechanics needs to be emphasized?
  - Fighting simulation

## Short description

- What is the game (with few words)?
  - Fighting against ai and other players

- What kind of game is it?
  - tekken like game

- What does the player do?
  - smashes the buttons and wins

- What is the core idea, slogan or basic philosophy of the game?
  - To be a better player

- What makes the game interesting?
  - Name at least four main things in the game
     - Playing against real people
     - mechanism
     - Collecting different characters
     - Different combos
 
## Game development

- Why are you making this game?
  - Theres good markets on fight games right now

- What is the core motivator for the player in this game?
 - Being the best fighter and remembering patterns.

- What is the background of this game?
 - you are a fighter and you need to fight to save the world

- What are the most prioritized goals for the development?
 - Make it fun to play and simple graphics

- Other goals you want to set for your team?
 - Make game very smooth running

- What is the unique part in this game that other games don’t have?
 - unique characters

## Target group

- What kind of gamers play this game?
 - tekken players

- Why do they want this game?
 - Because of its new tactics

- What kind of games do they like?
 - fighting games

- Is there any other reasons why the target group is important?
 - They give lot of good feedback about balance of the game.

## Methods

- How will you develop the game?
 - With team who has knowledge about fighting games and probably using some good engine

- Core methods, tools and/or softwares
 - simple 3d engine

- What technology are you using? (Game engine, etc)
 - Unity 5 maybe or source engine.

- Does the technology affect the development? (Why and how?)
 - Not really. 

- Who is developing the game and what parts of it?
 - Pauli Kokkonen

- How long does it take to make this game?
 - Probably couple years.
